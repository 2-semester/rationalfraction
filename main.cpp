#include <iostream>
#include "RationalFraction.h"
#include "RationalFraction.cpp"

using namespace std;

int main() {
    RationalFraction f1(1, 2);
    RationalFraction f2(3, 4);

    cout << "f1 = " << f1 << endl;
    cout << "f2 = " << f2 << endl;

    cout << "f1 + f2 = " << (f1 + f2) << endl;
    cout << "f1 - f2 = " << (f1 - f2) << endl;
    cout << "f1 * f2 = " << (f1 * f2) << endl;
    cout << "f1 / f2 = " << (f1 / f2) << endl;
}
#pragma once

#include <iostream>

class RationalFraction {
private:
    int numerator;
    int denominator;

    int gcd(int a, int b);
    void reduce();

public:
    RationalFraction(int num, int den);

    RationalFraction operator+(const RationalFraction& other);
    RationalFraction operator-(const RationalFraction& other);
    RationalFraction operator*(const RationalFraction& other);
    RationalFraction operator/(const RationalFraction& other);

    bool operator==(const RationalFraction& other);
    bool operator!=(const RationalFraction& other);

    friend std::ostream& operator<<(std::ostream& os, const RationalFraction& fraction);
};

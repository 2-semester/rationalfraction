#include "RationalFraction.h"
#include <iostream>

using namespace std;

int RationalFraction::gcd(int a, int b) {
    if (b == 0) {
        return a;
    }
    return gcd(b, a % b);
}

void RationalFraction::reduce() {
    int g = gcd(numerator, denominator);
    numerator /= g;
    denominator /= g;
}

RationalFraction::RationalFraction(int num, int den) {
    numerator = num;
    denominator = den;
    reduce();
}

RationalFraction RationalFraction::operator+(const RationalFraction& other) {
    int newNumerator = numerator * other.denominator + other.numerator * denominator;
    int newDenominator = denominator * other.denominator;
    return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::operator-(const RationalFraction& other) {
    int newNumerator = numerator * other.denominator - other.numerator * denominator;
    int newDenominator = denominator * other.denominator;
    return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::operator*(const RationalFraction& other) {
    int newNumerator = numerator * other.numerator;
    int newDenominator = denominator * other.denominator;
    return RationalFraction(newNumerator, newDenominator);
}

RationalFraction RationalFraction::operator/(const RationalFraction& other) {
    int newNumerator = numerator * other.denominator;
    int newDenominator = denominator * other.numerator;
    return RationalFraction(newNumerator, newDenominator);
}

bool RationalFraction::operator==(const RationalFraction& other) {
    return (numerator == other.numerator && denominator == other.denominator);
}

bool RationalFraction::operator!=(const RationalFraction& other) {
    return !(*this == other);
}

ostream& operator<<(ostream& os, const RationalFraction& fraction) {
    os << fraction.numerator << "/" << fraction.denominator;
    return os;
}